PROTOC_DOCKER_IMAGE=thethingsindustries/protoc
RUN_PROTOC=docker run --rm -v $(PWD):/app -w /app $(PROTOC_DOCKER_IMAGE)
COMPOSE=docker compose

pull-protoc-docker-image:
	@echo "🐳 : Pull docker image $(PROTOC_DOCKER_IMAGE)"
	@docker pull $(PROTOC_DOCKER_IMAGE)

pb: pull-protoc-docker-image
	@echo "🛠️  : Generate protobuf file and gRPC client mock"
	@$(RUN_PROTOC) --proto_path=api/ --go_out=plugins=grpc:. fb.proto
	@go generate ./pkg/pb/mock.go

gw: pull-protoc-docker-image
	@echo "🛠️ : Generate gateway file"
	@$(RUN_PROTOC) --proto_path=api/ --grpc-gateway_out=logtostderr=true:. fb.proto

swagger: pull-protoc-docker-image
	@echo "🚀 : Generate swagger file"
	@$(RUN_PROTOC) --proto_path=api/ --openapiv2_out=logtostderr=true:api/ fb.proto

test: ## Run unit tests
	go test -cover ./...

test-verbose: ## Run unit tests on verbose mode
	go test -v -cover ./...

.PHONY: coverage
coverage: ## Generate global code coverage report
	@chmod 777 ./tools/coverage.sh
	@./tools/coverage.sh;

.PHONY: build
build: 
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo $(LDFLAGS) gitlab.com/hmaiga/fb-service/cmd/fb-service

mock-update: 
	@echo "Update/Generate mocks"
	@go generate ./internal/...

ps: # Debug
	docker ps -a

up: # Debug
	$(COMPOSE) up -d

down: # Debug
	$(COMPOSE) down

restart: # Debug
	$(COMPOSE) down && $(COMPOSE) up -d

logs: # Debug
	docker logs fb-service