[![pipeline status](https://gitlab.com/hmaiga/fb-service/badges/main/pipeline.svg)](https://gitlab.com/hmaiga/fb-service/-/commits/main)
[![coverage report](https://gitlab.com/hmaiga/fb-service/badges/main/coverage.svg)](https://gitlab.com/hmaiga/fb-service/-/commits/main)

# Assignment

The original fizz-buzz consists in writing all numbers from 1 to 100, and just replacing all multiples of 3 by "fizz", all multiples of 5 by "buzz", and all multiples of 15 by "fizzbuzz".

The output would look like this: 
**"1,2,fizz,4,buzz,fizz,7,8,fizz,buzz,11,fizz,13,14,fizzbuzz,16,..."**.

Your goal is to implement a web server that will expose a REST API endpoint that:

- Accepts five parameters : three integers **int1**, **int2** and **limit**, and two strings **str1** and **str2**.
- Returns a list of strings with numbers from 1 to **limit**, where: all multiples of **int1** are replaced by **str1**, all multiples of **int2** are replaced by **str2**, all multiples of **int1** and **int2** are replaced by **str1str2**.

The server needs to be:

- Ready for production
- Easy to maintain by other developers

Bonus: add a statistics endpoint allowing users to know what the most frequent request has been. This endpoint should:

- Accept no parameter
- Return the parameters corresponding to the most used request, as well as the number of hits for this request

## Prerequisites

- Golang  (1.17)
- git
- docker / docker-compose
- make

## You have a working Docker environment

```bash
git clone https://gitlab.com/hmaiga/fb-service.git
cd fb-service
cp ./config/config.toml.example ./config/config.toml  ## ENVIRONNENT Variale to run the project, can be change if needed
make up
````

### Solution

`fb-service` is a golang service that exposes HTTP endpoints and GRPC methods.

## How to start the service in a docker container

The fizz buzz service uses :

- redis : data caching

- Swagger UI : testing of the HTTP endpoints

- Redis-exporter, Prometheus and grafana : monitoring

In order to run all of those and the `fb-service` in docker containers, run the following command:

```docker
make up
```

or to restart the service

```docker
make restart
```

You are able to see the service logs with the following command:

```docker
make logs
```

Make sure the service is running by opening your internet browser and typing the URL _http://localhost:3000/api/life_

The message `Welcome to the fizzbuzz service!` should be displayed.

## Swagger UI

The swagger ui allows to easily test the HTTP endpoints.

It is available at _http://localhost:5000_

## Prometheus & Grafana

Prometheus collect some metrics and Grafana render it on dashboards with graph or generate alerts.

Note that you can set Grafana administration username and password in by updating variables `GF_SECURITY_ADMIN_USER` and `GF_SECURITY_ADMIN_PASSWORD`. If you don’t, the default username and password are admin for both.

Prometheus is available at _http://localhost:9090_
Grafana is available at _http://localhost:9000_

### Follow these steps in order to see the existing dashboard

Redis Dashboard for Prometheus Redis Exporter 1.x

- Log in to Grafana as the administration user.
- In the left sidebar menu, select search dashboards.
- Search for dashboard name: `Redis Dashboard for Prometheus Redis Exporter 1.x` (Source: _https://grafana.com/grafana/dashboards/763_).
- Click on the dashboard and voila you can follow Redice instance metrics

## GRPC

### How to test

You can use any client you prefere in order to test the GRPC service (e.g BloomRPC).

## Generate the fb.pb.go file

This file is generated from `api/fb.proto`. It provides the client and server interfaces and instanciations for the GRPC service.

Once you modify the `.proto` file, you can generate `pkg/pb/fb.pb.go` with the following command:

```bash
make pb
```

## Generate the fb.pb.gw.go file

This file is generated from `api/fb.proto`. It provides the reverse proxy implementation used to consume the service in HTTP.

Once you modify the `.proto` file, you can generate `pkg/pb/fb.pb.gw.go` with the following command:

```bash
make gw
```

## Generate the fb.swagger.json file

This file is generated from `api/fb.proto`. It provides the service API definition and it is used by swagger UI.

Once you modify the `.proto` file, you can generate `api/fb.swagger.json` with the following command:

```bash
make swagger
```

## Generate the pb, gw and swagger files

All three files can be generated with only one command:

```bash
make all
```

# Contributing

You are more than welcome to submit issues and merge requests to this project.