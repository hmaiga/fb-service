package main

import (
	"context"
	"fmt"
	log_std "log"
	"net"
	"net/http"
	"os"

	"gitlab.com/hmaiga/fb-service/internal/config"
	"gitlab.com/hmaiga/fb-service/internal/fizzbuzz"
	fizzbuzz_repository "gitlab.com/hmaiga/fb-service/internal/fizzbuzz/repository"
	fizzbuzz_usecase "gitlab.com/hmaiga/fb-service/internal/fizzbuzz/usecase"
	fizzbuzz_service "gitlab.com/hmaiga/fb-service/internal/service"
	"gitlab.com/hmaiga/fb-service/pkg/pb"
	log "gitlab.com/hmaiga/fb-service/tools"
	"golang.org/x/sync/errgroup"

	"github.com/go-redis/redis/v8"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	_ "github.com/lib/pq"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/soheilhy/cmux"
	"google.golang.org/protobuf/encoding/protojson"

	"gitlab.com/hmaiga/fb-service/middleware"
	"google.golang.org/grpc"
)

func main() {
	// Context
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	// Logger
	logger, err := log.NewFBLogger(log.Level(config.LoggerLevel))
	if err != nil {
		log_std.Println("Error while initializing the logger: ", err.Error())
		os.Exit(1)
	}

	// Setup redis connection
	var rdb *redis.Client = redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%s", config.RedisHost, config.RedisPort),
		Password: config.RedisPassword,
		DB:       config.RedisBucketId,
	})

	if err := rdb.Ping(ctx).Err(); err != nil {
		fmt.Printf("Error while pinging the redis intance(%s:%s) : %s", config.RedisHost, config.RedisPort, err.Error())
		os.Exit(1)
	}

	// Init repositories
	fizzbuzzRepo := fizzbuzz_repository.NewRedisRepository(rdb)

	// Init usecases
	fizzbuzzUsecase := fizzbuzz_usecase.NewUsecase(logger, fizzbuzzRepo)

	// TCP Listener
	listener, err := net.Listen("tcp", ":"+config.Port)
	if err != nil {
		logger.Error(fmt.Sprintf("Error while starting the TCP listener : %s", err.Error()))
		os.Exit(1)
	}

	// Connection multiplexer
	m := cmux.New(listener)
	grpcListener := m.MatchWithWriters(cmux.HTTP2MatchHeaderFieldSendSettings("content-type", "application/grpc"))
	httpListener := m.Match(cmux.HTTP1Fast()) // cmux.HTTP2()

	g := new(errgroup.Group)

	g.Go(func() error { return grpcServe(ctx, grpcListener, logger, fizzbuzzUsecase) })
	g.Go(func() error { return httpServe(ctx, httpListener, logger) })
	g.Go(func() error { return m.Serve() })

	logger.Info(fmt.Sprintf("The fizz buzz service has been started on port %s", config.Port))

	log_std.Fatal(g.Wait())
}

func grpcServe(ctx context.Context, l net.Listener, logger log.Logger, fbuc fizzbuzz.Usecase) error {
	// Init fizzbuzz service
	fizzbuzzService := fizzbuzz_service.NewServer(logger, fbuc)

	// GRPC server
	grpcServer := grpc.NewServer(
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			grpc_prometheus.UnaryServerInterceptor,
		)),
	)

	pb.RegisterFizzBuzzServiceServer(grpcServer, fizzbuzzService)

	grpc_prometheus.Register(grpcServer)

	return grpcServer.Serve(l)
}

func httpServe(ctx context.Context, l net.Listener, logger log.Logger) error {
	mux := http.NewServeMux()

	// This marshaler is used for two things:
	// UseEnumNumbers: 		if we return an enum type field, we return the label and not the int value
	// EmitUnpopulated:		if a field value is "zero value" (example: for int its 0, for string its ""), we still return the value.
	//						Without this the field will not be returned in the response
	mar := runtime.JSONPb{
		MarshalOptions: protojson.MarshalOptions{
			UseEnumNumbers:  false,
			EmitUnpopulated: true,
		},
	}

	// request multiplexer for grpc-gateway
	gwmux := runtime.NewServeMux(runtime.WithMarshalerOption(runtime.MIMEWildcard, &mar))

	// HTTP endpoints
	opts := []grpc.DialOption{
		grpc.WithDefaultCallOptions(
			grpc.MaxCallRecvMsgSize(256<<32),
			grpc.MaxCallSendMsgSize(256<<32)),
		grpc.WithInsecure()}
	err := pb.RegisterFizzBuzzServiceHandlerFromEndpoint(ctx, gwmux, ":"+config.Port, opts)
	if err != nil {
		logger.Error(fmt.Sprintf("Error while registering endpoints : %s", err.Error()))
		return err
	}

	// Prometheus Metrics
	mux.Handle("/metrics", promhttp.Handler())

	// Swagger
	mux.Handle("/swagger", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		http.ServeFile(w, r, "api/fb.swagger.json")
	}))

	// Cors
	handlerEnriched := middleware.Cors(gwmux)
	mux.Handle("/", handlerEnriched)

	// HTTP server
	s := &http.Server{Handler: mux}
	return s.Serve(l)
}
