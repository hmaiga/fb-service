package config

import (
	"os"
	"path/filepath"

	"github.com/spf13/viper"
)

var (
	// Port : exposed port
	Port string

	// LoggerLevel : logger level
	LoggerLevel string

	// RedisHost : redis host
	RedisHost string

	// RedisPort : redis port
	RedisPort string

	// RedisBucketId : redis bucket identifier
	RedisBucketId int

	// RedisPassword : Redis password
	RedisPassword string
)

func init() {
	New()
}

//New : creates the service's configuration
func New() {
	viper.AutomaticEnv()

	if os.Getenv("ENVIRONMENT") == "DEV" {
		configFilePath := filepath.Dir("./config/config.toml")
		viper.SetConfigName("config")
		viper.SetConfigType("toml")
		viper.AddConfigPath(configFilePath)
		viper.ReadInConfig()
	}

	//Assign env variables value to global variables
	viper.SetDefault("PORT", "3000")
	viper.SetDefault("LOGGER_LEVEL", "warning")

	Port = viper.GetString("PORT")
	LoggerLevel = viper.GetString("LOGGER_LEVEL")
	RedisHost = viper.GetString("REDIS_HOST")
	RedisPort = viper.GetString("REDIS_PORT")
	RedisBucketId = viper.GetInt("REDIS_BUCKET_ID")
	RedisPassword = viper.GetString("REDIS_PASSWORD")

}
