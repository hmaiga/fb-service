package config

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

var portConfigTests = []struct {
	key   string
	value string
	out   string
	env   string
	desc  string
}{
	{"PORT", "", "3000", "DEV", "Default port configuration expect"},
}

func TestNew(t *testing.T) {
	for _, test := range portConfigTests {
		// Arrange
		os.Setenv("ENVIRONMENT", test.env)

		//Act
		New()

		//Assert
		assert.Equal(t, test.out, Port, test.desc)
	}
}
