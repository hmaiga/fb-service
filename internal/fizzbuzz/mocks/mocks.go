package mocks

// Usecase mocks
//go:generate go run github.com/golang/mock/mockgen -package=mocks -destination=usecase.go gitlab.com/hmaiga/fb-service/internal/fizzbuzz Usecase

// Repository mocks
//go:generate go run github.com/golang/mock/mockgen -package=mocks -destination=repository.go gitlab.com/hmaiga/fb-service/internal/fizzbuzz Repository
