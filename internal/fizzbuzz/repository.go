package fizzbuzz

import (
	"context"

	"gitlab.com/hmaiga/fb-service/internal/model"
)

// Repository represents the fizzbuzz's repository interface
type Repository interface {
	IncreaseRequestCounter(ctx context.Context, requestParams string, incrValue float64) (err error)
	GetMostHittedRequest(ctx context.Context) (*model.RequestWithScore, error)
}
