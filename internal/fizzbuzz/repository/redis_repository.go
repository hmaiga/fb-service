package repository

import (
	"context"
	"errors"
	"fmt"

	"github.com/go-redis/redis/v8"
	"gitlab.com/hmaiga/fb-service/internal/fizzbuzz"
	"gitlab.com/hmaiga/fb-service/internal/model"
)

const (
	requestHitsKey = "RequestHitsKey"
)

type redisRepository struct {
	conn *redis.Client
}

// NewRedisRepository : creates a new redis repository struct that implements the fizzbuzz.Repository interface
func NewRedisRepository(c *redis.Client) fizzbuzz.Repository {
	return &redisRepository{
		conn: c,
	}
}

// IncreaseRequestCounter : increase by 1 the current request key
// it returns err if requestParams is empty otherwise nil
func (r *redisRepository) IncreaseRequestCounter(ctx context.Context, requestParams string, incrValue float64) error {
	if len(requestParams) == 0 || incrValue == 0 {
		return errors.New("invalid input: request params should not be zero value")
	}
	if _, err := r.conn.ZIncrBy(ctx, requestHitsKey, incrValue, requestParams).Result(); err != nil {
		return err
	}

	return nil
}

// GetMostHittedRequest returns the most hitted request with his score
func (r *redisRepository) GetMostHittedRequest(ctx context.Context) (*model.RequestWithScore, error) {
	if c, check_err := r.conn.Exists(ctx, requestHitsKey).Result(); check_err != nil {
		return nil, fmt.Errorf("internal error: redis instance is not available: %s", check_err.Error())
	} else if c == 0 {
		return nil, errors.New("empty db: there is no data corresponding to this request yet")
	}

	reqWithScores, z_err := r.conn.ZRevRangeWithScores(ctx, requestHitsKey, 0, -1).Result()
	if z_err != nil {
		return nil, fmt.Errorf("internal error: failed to get the most hitted request: %s", z_err.Error())
	}

	key, ok := reqWithScores[0].Member.(string)
	if !ok {
		return nil, fmt.Errorf("failed to get the most hitted request")
	}

	return &model.RequestWithScore{RequestParams: key, Hits: int64(reqWithScores[0].Score)}, nil

}
