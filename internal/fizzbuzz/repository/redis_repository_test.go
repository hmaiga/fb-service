package repository

import (
	"context"
	"errors"
	"fmt"
	"testing"

	"github.com/go-redis/redis/v8"
	"github.com/go-redis/redismock/v8"
	"github.com/stretchr/testify/assert"
	"gitlab.com/hmaiga/fb-service/internal/model"
)

func Test_IncreaseRequestCounter(t *testing.T) {
	ctx := context.Background()
	requestHitsKey := "RequestHitsKey"

	// Arrange Test cases
	var tests = []struct {
		description   string
		requestParams string
		incrBy        float64
		ZIncrByErr    error
		expectedErr   error
	}{
		{
			description: "Failure: invalid input, zero value",
			expectedErr: errors.New("invalid input: request params should not be zero value"),
		},
		{
			description:   "Failure: cannot increase number of hits",
			requestParams: "Test",
			incrBy:        1,
			ZIncrByErr:    errors.New("failed to increase request hits"),
			expectedErr:   errors.New("failed to increase request hits"),
		},
		{
			description:   "Success: should increase number of hits",
			requestParams: "Test",
			incrBy:        1,
		},
	}

	for _, tc := range tests {
		t.Run(tc.description, func(t *testing.T) {
			// Arrange
			db, mock := redismock.NewClientMock()
			redisRepo := NewRedisRepository(db)

			// Expectations
			if tc.ZIncrByErr != nil {
				mock.ExpectZIncrBy(requestHitsKey, tc.incrBy, tc.requestParams).SetErr(tc.ZIncrByErr)
			} else {
				mock.ExpectZIncrBy(requestHitsKey, tc.incrBy, tc.requestParams).SetVal(float64(1))
			}

			// Act
			actualErr := redisRepo.IncreaseRequestCounter(ctx, tc.requestParams, tc.incrBy)

			// Assert
			assert.Equal(t, tc.expectedErr, actualErr, tc.description)
		})
	}
}

func Test_GetMostHittedRequest(t *testing.T) {
	ctx := context.Background()
	requestHitsKey := "RequestHitsKey"

	// Arrange Test cases
	var tests = []struct {
		description             string
		rExistsErr              error
		rExistsResp             int64
		zRevRangeWithScoresErr  error
		zRevRangeWithScoresResp []redis.Z
		expectedErr             error
		expectedResp            *model.RequestWithScore
	}{
		{
			description: "Failure: service unavailable",
			rExistsErr:  errors.New("KO"),
			expectedErr: fmt.Errorf("internal error: redis instance is not available: KO"),
		},
		{
			description: "Failure: cache key does not exist",
			rExistsResp: int64(0),
			expectedErr: errors.New("empty db: there is no data corresponding to this request yet"),
		},
		{
			description:            "Failure: Unable to retrieve cache data",
			rExistsResp:            int64(1),
			zRevRangeWithScoresErr: errors.New("KO"),
			expectedErr:            fmt.Errorf("internal error: failed to get the most hitted request: KO"),
		},
		{
			description:             "Failure: empty cache for the given key",
			rExistsResp:             int64(1),
			zRevRangeWithScoresResp: []redis.Z{{Score: 1}},
			expectedErr:             fmt.Errorf("failed to get the most hitted request"),
		},
		{
			description:             "Success: should retrieve",
			rExistsResp:             int64(1),
			zRevRangeWithScoresResp: []redis.Z{{Member: "1__2__3__f__b", Score: 5}},
			expectedResp:            &model.RequestWithScore{RequestParams: "1__2__3__f__b", Hits: 5},
		},
	}

	for _, tc := range tests {
		t.Run(tc.description, func(t *testing.T) {
			// Arrange
			db, mock := redismock.NewClientMock()
			redisRepo := NewRedisRepository(db)

			// Expectations
			if tc.rExistsErr != nil {
				mock.ExpectExists(requestHitsKey).SetErr(tc.rExistsErr)
			} else {
				mock.ExpectExists(requestHitsKey).SetVal(tc.rExistsResp)
			}

			if tc.zRevRangeWithScoresErr != nil {
				mock.ExpectZRevRangeWithScores(requestHitsKey, 0, -1).SetErr(tc.zRevRangeWithScoresErr)
			} else {
				mock.ExpectZRevRangeWithScores(requestHitsKey, 0, -1).SetVal(tc.zRevRangeWithScoresResp)
			}

			// Act
			actualResp, actualErr := redisRepo.GetMostHittedRequest(ctx)

			// Assert
			assert.Equal(t, tc.expectedErr, actualErr, tc.description)
			assert.Equal(t, tc.expectedResp, actualResp, tc.description)
		})
	}
}
