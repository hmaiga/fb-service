package fizzbuzz

import (
	"context"

	"gitlab.com/hmaiga/fb-service/internal/model"
)

// Usecase represents the fizzbuzz's usecase interface
type Usecase interface {
	GetFizzBuzz(ctx context.Context, params *model.FizzBuzzRequest) ([]string, error)
	IncreaseRequestNumberOfHits(ctx context.Context, params *model.FizzBuzzRequest) error
	GetStatistics(ctx context.Context) (*model.FizzBuzzStats, error)
}
