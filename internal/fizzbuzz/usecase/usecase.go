package usecase

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/hmaiga/fb-service/internal/fizzbuzz"
	"gitlab.com/hmaiga/fb-service/internal/model"
	log "gitlab.com/hmaiga/fb-service/tools"
)

const (
	min             int64 = 1
	max             int64 = 1_000_000
	reservedKeyword       = "__"
	increaseHitBy         = 1
)

type usecase struct {
	logger log.Logger
	repo   fizzbuzz.Repository
}

// NewUsecase : creates a new usecase struct that implements the fizzbuzz.Usecase interface
func NewUsecase(l log.Logger, r fizzbuzz.Repository) fizzbuzz.Usecase {
	return &usecase{
		logger: l,
		repo:   r,
	}
}

// isIndexDivisibleBy returns true the input1 is divisible by input2 otherwise false
func isIndexDivisibleBy(input1 int64, input2 int64) bool {
	// Avoid illegal operation, return false while trying to divide by zero
	if input2 == 0 {
		return false
	}
	return input1%input2 == 0
}

// GetFizzBuzz returns a list of strings with numbers from 1 to limit, where:
//
// - all multiples of int1 and int2 are replaced by str1str2
//
// - all multiples of int1 are replaced by str1,
//
// - all multiples of int2 are replaced by str2.
//
// Notice: By configuration, the limit cannot exceed 1_000_000 but it can be change if needed
func (uc *usecase) GetFizzBuzz(ctx context.Context, params *model.FizzBuzzRequest) ([]string, error) {

	if params.GetLimit() < min || params.GetLimit() > max {
		return nil, fmt.Errorf("invalid input(limit=%d): the limit must be between %d and %d", params.GetLimit(), min, max)
	}

	var fbResult []string
	for i := int64(1); i <= params.GetLimit(); i++ {
		int1Ok := isIndexDivisibleBy(i, params.GetInt1())
		int2Ok := isIndexDivisibleBy(i, params.GetInt2())
		if int1Ok && int2Ok {
			fbResult = append(fbResult, params.GetStr1()+params.GetStr2())
		} else if int1Ok {
			fbResult = append(fbResult, params.GetStr1())
		} else if int2Ok {
			fbResult = append(fbResult, params.GetStr2())
		} else {
			fbResult = append(fbResult, strconv.FormatInt(i, 10))
		}
	}

	return fbResult, nil
}

// GetStatistics returns the parameters corresponding to the most used request,
// as well as the number of hits for this request
func (uc *usecase) GetStatistics(ctx context.Context) (*model.FizzBuzzStats, error) {
	reqWithScore, err := uc.repo.GetMostHittedRequest(ctx)
	if err != nil {
		return nil, err
	}
	requestParams := strings.Split(reqWithScore.RequestParams, reservedKeyword)
	if len(requestParams) != 5 {
		return nil, fmt.Errorf("unexpected request param length: %d for %s", len(requestParams), reqWithScore.RequestParams)
	}
	int1, int2, limit, p_err := getParsedIntParams(requestParams)
	if p_err != nil {
		return nil, p_err
	}
	return &model.FizzBuzzStats{
		NumberOfHits: reqWithScore.Hits,
		Int1:         int1,
		Int2:         int2,
		Limit:        limit,
		Str1:         requestParams[3],
		Str2:         requestParams[4],
	}, nil
}

// IncreaseRequestNumberOfHits returns error if failed to increase number of hits otherwise nil
func (uc *usecase) IncreaseRequestNumberOfHits(ctx context.Context, params *model.FizzBuzzRequest) error {
	requestParams := fmt.Sprintf(
		"%[1]d%[6]s%[2]d%[6]s%[3]d%[6]s%[4]s%[6]s%[5]s",
		params.GetInt1(),
		params.GetInt2(),
		params.GetLimit(),
		params.GetStr1(),
		params.GetStr2(),
		reservedKeyword)
	return uc.repo.IncreaseRequestCounter(ctx, requestParams, increaseHitBy)
}

// getParsedIntParams: returns parsed request int params
func getParsedIntParams(params []string) (int1, int2, limit int64, err error) {
	parseInt64 := func(param string) (int64, error) { return strconv.ParseInt(param, 10, 64) }
	int1, err1 := parseInt64(params[0])
	int2, err2 := parseInt64(params[1])
	limit, err3 := parseInt64(params[2])
	if err1 != nil || err2 != nil || err3 != nil {
		err = fmt.Errorf("failed to parse request params: %v", params)
	}
	return
}
