package usecase

import (
	"context"
	"fmt"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/hmaiga/fb-service/internal/fizzbuzz/mocks"
	"gitlab.com/hmaiga/fb-service/internal/model"
	log "gitlab.com/hmaiga/fb-service/tools"
)

func getTestLogger() log.Logger {
	testLogger, _ := log.NewFBLogger(log.Level("fatal"))

	if testing.Verbose() {
		testLogger, _ = log.NewFBLogger(log.Level("debug"))
	}

	return testLogger
}

func Test_NewUsecase(t *testing.T) {
	// Arrange
	logger := getTestLogger()
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	repo := &mocks.MockRepository{}

	expected := &usecase{
		logger: logger,
		repo:   repo,
	}

	// Act
	actual := NewUsecase(logger, repo)

	// Assert
	assert.Equal(t, expected, actual)
}

func Test_isIndexDivisibleBy(t *testing.T) {
	// Arrange Test cases
	var tests = []struct {
		description string
		input1      int64
		input2      int64
		expectedRes bool
	}{
		{
			description: "Divide by zero: Should return false when input2 is zero",
			input2:      0,
			expectedRes: false,
		},
		{
			description: "Should return false when input1 is not divible by input2",
			input1:      3,
			input2:      2,
			expectedRes: false,
		},
		{
			description: "Should return true when divisible",
			input1:      4,
			input2:      2,
			expectedRes: true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.description, func(t *testing.T) {
			// Act
			actualRes := isIndexDivisibleBy(tc.input1, tc.input2)

			// Assert
			assert.Equal(t, tc.expectedRes, actualRes, tc.description)
		})
	}
}

func Test_GetFizzBuzz(t *testing.T) {
	ctx := context.Background()
	logger := getTestLogger()

	// Test cases
	var tests = []struct {
		description   string
		requestParams *model.FizzBuzzRequest
		IncrParam     string
		IncrTimes     int32
		expectedRes   []string
		expectedErr   error
	}{
		{
			description: "Failure: error when limit value is less than 1",
			expectedErr: fmt.Errorf("invalid input(limit=%d): the limit must be between %d and %d", 0, 1, 1_000_000),
		},
		{
			description:   "Failure: error when limit value is greater than 1_000_000",
			requestParams: &model.FizzBuzzRequest{Limit: 1_000_001},
			expectedErr:   fmt.Errorf("invalid input(limit=%d): the limit must be between %d and %d", 1_000_001, 1, 1_000_000),
		},
		{
			description:   "Success: valid input",
			requestParams: &model.FizzBuzzRequest{Int1: 2, Int2: 3, Limit: 6, Str1: "fizz", Str2: "buzz"},
			IncrParam:     "2__3__6__fizz__buzz",
			IncrTimes:     1,
			expectedRes:   []string{"1", "fizz", "buzz", "fizz", "5", "fizzbuzz"},
		},
	}

	for _, tc := range tests {
		t.Run(tc.description, func(t *testing.T) {
			// Arrange
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			mockRepository := mocks.NewMockRepository(ctrl)

			uc := NewUsecase(logger, mockRepository)

			// Act
			actual, err := uc.GetFizzBuzz(ctx, tc.requestParams)

			// Assert
			assert.Equal(t, tc.expectedRes, actual, tc.description)
			assert.Equal(t, tc.expectedErr, err, tc.description)
		})
	}
}

func Test_GetStatistics(t *testing.T) {
	ctx := context.Background()
	logger := getTestLogger()

	// Test cases
	var tests = []struct {
		description              string
		GetMostHittedRequestResp *model.RequestWithScore
		GetMostHittedRequestErr  error
		expectedRes              *model.FizzBuzzStats
		expectedErr              error
	}{
		{
			description:             "Failure: error to retrieve most hitted request",
			GetMostHittedRequestErr: fmt.Errorf("error to retrieve most hitted request"),
			expectedErr:             fmt.Errorf("error to retrieve most hitted request"),
		},
		{
			description:              "Failure: unexpected request param length",
			GetMostHittedRequestResp: &model.RequestWithScore{RequestParams: "1__8__f"},
			expectedErr:              fmt.Errorf("unexpected request param length: %d for %s", 3, "1__8__f"),
		},
		{
			description:              "Failure: unexpected request param",
			GetMostHittedRequestResp: &model.RequestWithScore{RequestParams: "1__8__A__f__b"},
			expectedErr:              fmt.Errorf("failed to parse request params: %v", []string{"1", "8", "A", "f", "b"}),
		},
		{
			description:              "Success: should return the most hitted request and score",
			GetMostHittedRequestResp: &model.RequestWithScore{RequestParams: "1__8__3__f__b", Hits: 5},
			expectedRes:              &model.FizzBuzzStats{NumberOfHits: 5, Int1: 1, Int2: 8, Limit: 3, Str1: "f", Str2: "b"},
		},
	}

	for _, tc := range tests {
		t.Run(tc.description, func(t *testing.T) {
			// Arrange
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			mockRepository := mocks.NewMockRepository(ctrl)

			// Expectations
			mockRepository.EXPECT().GetMostHittedRequest(ctx).Return(tc.GetMostHittedRequestResp, tc.GetMostHittedRequestErr)

			uc := NewUsecase(logger, mockRepository)

			// Act
			actual, err := uc.GetStatistics(ctx)

			// Assert
			assert.Equal(t, tc.expectedRes, actual, tc.description)
			assert.Equal(t, tc.expectedErr, err, tc.description)
		})
	}
}

func Test_IncreaseRequestNumberOfHits(t *testing.T) {
	ctx := context.Background()
	logger := getTestLogger()

	// Test cases
	var tests = []struct {
		description   string
		requestParams *model.FizzBuzzRequest
		IncrParam     string
		expectedErr   error
	}{
		{
			description:   "Failure: error while increasing request hit",
			requestParams: &model.FizzBuzzRequest{Int1: 2, Int2: 3, Limit: 6, Str1: "fizz", Str2: "buzz"},
			IncrParam:     "2__3__6__fizz__buzz",
			expectedErr:   fmt.Errorf("error while increasing hit"),
		},
		{
			description:   "Success: request hit should be increased",
			requestParams: &model.FizzBuzzRequest{Int1: 2, Int2: 3, Limit: 6, Str1: "fizz", Str2: "buzz"},
			IncrParam:     "2__3__6__fizz__buzz",
		},
	}

	for _, tc := range tests {
		t.Run(tc.description, func(t *testing.T) {
			// Arrange
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			mockRepository := mocks.NewMockRepository(ctrl)

			// Expectations
			mockRepository.EXPECT().IncreaseRequestCounter(ctx, tc.IncrParam, float64(1)).Return(tc.expectedErr)

			uc := NewUsecase(logger, mockRepository)

			// Act
			err := uc.IncreaseRequestNumberOfHits(ctx, tc.requestParams)

			// Assert
			assert.Equal(t, tc.expectedErr, err, tc.description)
		})
	}
}
