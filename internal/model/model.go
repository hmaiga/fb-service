package model

import "gitlab.com/hmaiga/fb-service/pkg/pb"

// FizzBuzzRequest is an alias of protobuf GetFizzBuzzRequest as we use protobuf model
type FizzBuzzRequest = pb.GetFizzBuzzRequest

// FizzBuzzResponse is an alias of protobuf GetFizzBuzzResponse as we use protobuf model
type FizzBuzzResponse = pb.GetFizzBuzzResponse

// FizzBuzzStats is an alias of protobuf GetStatisticsResponse as we use protobuf model
type FizzBuzzStats = pb.GetStatisticsResponse

// RequestWithScore : request with score
type RequestWithScore struct {
	RequestParams string
	Hits          int64
}
