package service

import (
	"context"
	"fmt"

	"gitlab.com/hmaiga/fb-service/pkg/pb"

	"gitlab.com/hmaiga/fb-service/internal/fizzbuzz"
	log "gitlab.com/hmaiga/fb-service/tools"
)

// Server : represents the fizzbuzz service server
type Server struct {
	logger          log.Logger
	fizzbuzzUsecase fizzbuzz.Usecase
}

// NewServer creates a grpc server for the FizzBuzz service
func NewServer(l log.Logger, suc fizzbuzz.Usecase) *Server {
	return &Server{
		logger:          l,
		fizzbuzzUsecase: suc,
	}
}

const (
	welcomeMessage = "Welcome to the fizzbuzz service!"
)

// IsFizzBuzzServiceAlive returns a welcome message.
func (s *Server) IsFizzBuzzServiceAlive(ctx context.Context, req *pb.Empty) (*pb.IsFizzBuzzServiceAliveResponse, error) {
	return &pb.IsFizzBuzzServiceAliveResponse{WelcomeMessage: welcomeMessage}, nil
}

func (s *Server) GetFizzBuzz(ctx context.Context, fbReq *pb.GetFizzBuzzRequest) (*pb.GetFizzBuzzResponse, error) {

	err := s.fizzbuzzUsecase.IncreaseRequestNumberOfHits(ctx, fbReq)
	if err != nil {
		s.logger.Error(fmt.Sprintf("An error occurred while increasing request number of hits: %s", err.Error()))
		return nil, err
	}

	fb, err := s.fizzbuzzUsecase.GetFizzBuzz(ctx, fbReq)

	return &pb.GetFizzBuzzResponse{Result: fb}, err
}

// GetStatistics returns fizzbuzz most hitted request parameters
func (s *Server) GetStatistics(ctx context.Context, statReq *pb.Empty) (*pb.GetStatisticsResponse, error) {
	return s.fizzbuzzUsecase.GetStatistics(ctx)
}
