package service

import (
	"context"
	"fmt"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/hmaiga/fb-service/internal/fizzbuzz/mocks"
	"gitlab.com/hmaiga/fb-service/pkg/pb"
	log "gitlab.com/hmaiga/fb-service/tools"
)

func getTestLogger() log.Logger {
	testLogger, _ := log.NewFBLogger(log.Level("fatal"))

	if testing.Verbose() {
		testLogger, _ = log.NewFBLogger(log.Level("debug"))
	}

	return testLogger
}

func Test_NewServer(t *testing.T) {
	// Arrange
	logger := getTestLogger()
	mocks := &mocks.MockUsecase{}

	// Act
	actualServer := NewServer(logger, mocks)

	// Assert
	assert.NotEqual(t, actualServer, nil)
}

func Test_GetFizzBuzz(t *testing.T) {
	ctx := context.Background()
	logger := getTestLogger()

	// Test cases
	var tests = []struct {
		description   string
		requestParams *pb.GetFizzBuzzRequest
		HitIncErr     error
		HitIncTimes   int
		fbUsRes       []string
		fbUsErr       error
		fbUsTimes     int
		expectedRes   *pb.GetFizzBuzzResponse
		expectedErr   error
	}{
		{
			description: "Failure: increase request number of hits",
			HitIncErr:   fmt.Errorf("fail to increase request number of hits"),
			fbUsTimes:   0,
			expectedErr: fmt.Errorf("fail to increase request number of hits"),
		},
		{
			description: "Failure: get fizz buzz response",
			HitIncErr:   nil,
			fbUsTimes:   1,
			fbUsErr:     fmt.Errorf("fail to get fizzbuzz response"),
			expectedErr: fmt.Errorf("fail to get fizzbuzz response"),
			expectedRes: &pb.GetFizzBuzzResponse{},
		},
		{
			description:   "Success: should increase and return valid fizz buzz response",
			requestParams: &pb.GetFizzBuzzRequest{Int1: 2, Int2: 3, Limit: 6, Str1: "fizz", Str2: "buzz"},
			fbUsTimes:     1,
			HitIncErr:     nil,
			fbUsRes:       []string{"1", "fizz", "buzz", "fizz", "5", "fizzbuzz"},
			expectedRes:   &pb.GetFizzBuzzResponse{Result: []string{"1", "fizz", "buzz", "fizz", "5", "fizzbuzz"}},
		},
	}

	for _, tc := range tests {
		t.Run(tc.description, func(t *testing.T) {
			// Arrange
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			mockUsecase := mocks.NewMockUsecase(ctrl)

			// Expectations
			mockUsecase.EXPECT().IncreaseRequestNumberOfHits(ctx, tc.requestParams).Return(tc.HitIncErr)
			mockUsecase.EXPECT().GetFizzBuzz(ctx, tc.requestParams).Times(tc.fbUsTimes).Return(tc.fbUsRes, tc.fbUsErr)

			s := NewServer(logger, mockUsecase)

			// Act
			actual, err := s.GetFizzBuzz(ctx, tc.requestParams)

			// Assert
			assert.Equal(t, tc.expectedRes, actual, tc.description)
			assert.Equal(t, tc.expectedErr, err, tc.description)
		})
	}
}
