package middleware

import (
	"net/http"
	"strings"
)

// Cors : cross-origin resource sharing
func Cors(next http.Handler) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		header := w.Header()

		o := r.Header.Get("Origin")
		header.Set("Access-Control-Allow-Origin", o)

		if r.Method == http.MethodOptions {
			header.Set("Access-Control-Allow-Methods", strings.Join(
				[]string{
					http.MethodOptions,
					http.MethodGet,
					http.MethodPut,
					http.MethodHead,
					http.MethodPost,
					http.MethodDelete,
					http.MethodPatch,
					http.MethodTrace,
				}, ", ",
			))

			header.Set("Access-Control-Allow-Headers", strings.Join(
				[]string{
					"Access-Control-Allow-Headers",
					"Origin",
					"X-Requested-With",
					"Content-Type",
					"Accept",
				}, ", ",
			))

			return
		}

		next.ServeHTTP(w, r)
	})
}
