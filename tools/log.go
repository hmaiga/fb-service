package log

import (
	"os"
	"strings"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// Level : converts a log level from string to zapcore.Level
func Level(level string) zapcore.Level {
	switch strings.ToLower(level) {
	case "debug":
		return zapcore.DebugLevel
	case "info":
		return zapcore.InfoLevel
	case "warn":
		return zapcore.WarnLevel
	case "error":
		return zapcore.ErrorLevel
	case "dpanic":
		return zapcore.DPanicLevel
	case "panic":
		return zapcore.PanicLevel
	case "fatal":
		return zapcore.FatalLevel
	default:
		return zapcore.InfoLevel
	}
}

type Logger interface {
	Info(msg string, fields ...zapcore.Field)
	Warn(msg string, fields ...zapcore.Field)
	Error(msg string, fields ...zapcore.Field)
	Debug(msg string, fields ...zapcore.Field)
	Fatal(msg string, fields ...zapcore.Field)
	Sync() error
}

type loggerWrapper struct {
	logger *zap.Logger
}

func newEncoderConfig() zapcore.EncoderConfig {
	return zapcore.EncoderConfig{
		TimeKey:       "timestamp",
		LevelKey:      "level",
		MessageKey:    "message",
		CallerKey:     "",
		NameKey:       "",
		StacktraceKey: "stackTrace",
		LineEnding:    zapcore.DefaultLineEnding,
		EncodeLevel:   zapcore.CapitalLevelEncoder,
		EncodeTime:    zapcore.ISO8601TimeEncoder,
		EncodeCaller:  zapcore.ShortCallerEncoder,
	}
}

// NewLogger : Creates a new logger
func NewFBLogger(level zapcore.Level) (Logger, error) {
	logger, err := newConfig(level, newEncoderConfig()).Build(
		zap.Fields(
			zap.String("productName", os.Getenv("PRODUCT_NAME")),
			zap.String("AppName", os.Getenv("APPLICATION_NAME")),
			zap.String("AppVersion", os.Getenv("APPLICATION_VERSION")),
			zap.String("serverName", os.Getenv("SERVER_NAME")),
		),
	)
	if err != nil {
		return nil, err
	}
	return &loggerWrapper{logger: logger}, nil
}

func newConfig(level zapcore.Level, encoderConfig zapcore.EncoderConfig) zap.Config {
	return zap.Config{
		Level:       zap.NewAtomicLevelAt(level),
		Development: false,
		Sampling: &zap.SamplingConfig{
			Initial:    100,
			Thereafter: 100,
		},
		Encoding:         "json",
		EncoderConfig:    encoderConfig,
		OutputPaths:      []string{"stdout"},
		ErrorOutputPaths: []string{"stdout"},
	}
}

func Type(val string) zapcore.Field {
	return zap.String("type", val)
}

func (lw *loggerWrapper) Info(msg string, fields ...zapcore.Field) {
	lw.logger.Info(msg,
		append(fields,
			Type("APPLICATION"),
		)...)
}

func (lw *loggerWrapper) Warn(msg string, fields ...zapcore.Field) {
	lw.logger.Warn(msg,
		append(fields,
			Type("APPLICATION"),
		)...)
}

func (lw *loggerWrapper) Error(msg string, fields ...zapcore.Field) {
	lw.logger.Error(msg,
		append(fields,
			Type("APPLICATION"),
		)...)
}

func (lw *loggerWrapper) Debug(msg string, fields ...zapcore.Field) {
	lw.logger.Debug(msg,
		append(fields,
			Type("APPLICATION"),
		)...)
}

func (lw *loggerWrapper) Fatal(msg string, fields ...zapcore.Field) {
	lw.logger.Fatal(msg,
		append(fields,
			Type("APPLICATION"),
		)...)
}

func (lw *loggerWrapper) Sync() error {
	return lw.logger.Sync()
}
