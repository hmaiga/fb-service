// +build tools

package tools

import (
	_ "github.com/golang/mock/mockgen"
	_ "google.golang.org/protobuf/reflect/protoreflect"
	_ "google.golang.org/protobuf/runtime/protoimpl"
)
